*****************************
           App Push
*****************************

About:
------
Send Push Notification messages to Firebase Cloud Messaging server.

References:
-----------
1. https://firebase.google.com/docs/cloud-messaging/chrome/client
2. https://firebase.google.com/docs/cloud-messaging/server#http-request
3. https://gist.github.com/prime31/5675017

Author:
-------
drupal.org: sarathkm
email: goldenvalley4ever@gmail.com